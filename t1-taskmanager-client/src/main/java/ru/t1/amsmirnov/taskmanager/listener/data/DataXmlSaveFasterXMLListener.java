package ru.t1.amsmirnov.taskmanager.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.dto.request.data.DataXmlSaveFasterXMLRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.data.DataXmlSaveFasterXMLResponse;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

@Component
public final class DataXmlSaveFasterXMLListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-save-xml";

    @NotNull
    public static final String DESCRIPTION = "Save data to XML file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@dataXmlSaveFasterXMLListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DATA SAVE XML]");
        @NotNull final DataXmlSaveFasterXMLRequest request = new DataXmlSaveFasterXMLRequest(getToken());
        @NotNull final DataXmlSaveFasterXMLResponse response = domainEndpoint.saveDataXmlFasterXML(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
