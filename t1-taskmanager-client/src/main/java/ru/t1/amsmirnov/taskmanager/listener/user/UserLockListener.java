package ru.t1.amsmirnov.taskmanager.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserLockRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.user.UserLockResponse;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

@Component
public final class UserLockListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "user-lock";

    @NotNull
    public static final String DESCRIPTION = "Lock user by login.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @EventListener(condition = "@userLockListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER USER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserLockRequest request = new UserLockRequest(getToken(), login);
        @NotNull final UserLockResponse response = userEndpoint.lockUserByLogin(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
