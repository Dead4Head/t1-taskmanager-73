package ru.t1.amsmirnov.taskmanager.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class DataBase64LoadRequest extends AbstractUserRequest {

    public DataBase64LoadRequest() {
    }

    public DataBase64LoadRequest(@Nullable final String token) {
        super(token);
    }

}
