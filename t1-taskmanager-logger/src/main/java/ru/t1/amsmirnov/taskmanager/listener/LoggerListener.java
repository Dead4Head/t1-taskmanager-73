package ru.t1.amsmirnov.taskmanager.listener;

import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.api.ILoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Component
public final class LoggerListener implements MessageListener {

    @NotNull
    private final static Logger logger = Logger.getLogger(LoggerListener.class);

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    public LoggerListener() {
    }

    @Override
    public void onMessage(final Message message) {
        try {
            if (!(message instanceof TextMessage)) return;
            @NotNull final TextMessage textMessage = (TextMessage) message;
            @NotNull final String yaml = textMessage.getText();
            loggerService.log(yaml);
        } catch (final Exception exception) {
            logger.error(exception);
        }
    }

}
