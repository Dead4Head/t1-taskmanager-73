package ru.t1.amsmirnov.taskmanager.configuration;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.t1.amsmirnov.taskmanager.api.service.IDBProperty;

import javax.sql.DataSource;
import java.util.Properties;

import static org.hibernate.cfg.AvailableSettings.*;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.t1.amsmirnov.taskmanager")
@EnableJpaRepositories("ru.t1.amsmirnov.taskmanager.repository")
public class ServerConfiguration {

    @NotNull
    @Autowired
    private IDBProperty dataBaseProperty;

    @Bean
    @NotNull
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(dataBaseProperty.getDatabaseDriver());
        dataSource.setUrl(dataBaseProperty.getDatabaseUrl());
        dataSource.setUsername(dataBaseProperty.getDatabaseUser());
        dataSource.setPassword(dataBaseProperty.getDatabasePassword());
        return dataSource;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(@NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull final DataSource dataSource) {
        final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.amsmirnov.taskmanager.dto", "ru.t1.amsmirnov.taskmanager.model");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, dataBaseProperty.getDatabaseDialect());
        properties.put(Environment.HBM2DDL_AUTO, dataBaseProperty.getDatabaseHbm2ddlAuto());
        properties.put(Environment.SHOW_SQL, dataBaseProperty.getDatabaseShowSql());
        properties.put(Environment.FORMAT_SQL, dataBaseProperty.getDatabaseFormatSql());
        properties.put(Environment.USE_SQL_COMMENTS, dataBaseProperty.getDatabaseCommentsSql());

        properties.put(USE_SECOND_LEVEL_CACHE, dataBaseProperty.getDataBaseSecondLvlCache());
        properties.put(USE_QUERY_CACHE, dataBaseProperty.getDataBaseUseQueryCache());
        properties.put(USE_MINIMAL_PUTS, dataBaseProperty.getDataBaseUseMinPuts());
        properties.put(CACHE_REGION_PREFIX, dataBaseProperty.getDataBaseRegionPrefix());
        properties.put(CACHE_REGION_FACTORY, dataBaseProperty.getDataBaseFactoryClass());
        properties.put(CACHE_PROVIDER_CONFIG, dataBaseProperty.getDataBaseHazelConfig());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

}
