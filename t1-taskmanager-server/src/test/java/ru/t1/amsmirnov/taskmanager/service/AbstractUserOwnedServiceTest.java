package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.amsmirnov.taskmanager.api.service.dto.IUserDtoService;
import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.service.dto.UserDtoService;

import java.util.UUID;

public abstract class AbstractUserOwnedServiceTest extends AbstractServiceTest {

    @NotNull
    protected static final String USER_ALFA_ID = UUID.randomUUID().toString();

    @NotNull
    protected static final String USER_BETA_ID = UUID.randomUUID().toString();

    @NotNull
    static final UserDTO USER_ALFA = new UserDTO();

    @NotNull
    static final UserDTO USER_BETA = new UserDTO();

    @NotNull
    protected IUserDtoService userDtoService;

    @BeforeClass
    public static void initUsers() throws Exception {
        USER_ALFA.setId(USER_ALFA_ID);
        USER_ALFA.setFirstName("First Name");
        USER_ALFA.setPasswordHash("HASH!!!!");
        USER_ALFA.setLastName("Last Name ");
        USER_ALFA.setMiddleName("Middle Name ");
        USER_ALFA.setEmail("useralfa@tm.ru");
        USER_ALFA.setLogin("alfa");
        USER_ALFA.setRole(Role.USUAL);

        USER_BETA.setId(USER_BETA_ID);
        USER_BETA.setFirstName("First Name");
        USER_BETA.setPasswordHash("HASH!!!!");
        USER_BETA.setLastName("Last Name ");
        USER_BETA.setMiddleName("Middle Name ");
        USER_BETA.setEmail("userbeta@tm.ru");
        USER_BETA.setLogin("beta");
        USER_BETA.setRole(Role.USUAL);
    }

    @Before
    public void addUsers() throws Exception {
        userDtoService = context.getBean(IUserDtoService.class);
        userDtoService.add(USER_ALFA);
        userDtoService.add(USER_BETA);
    }

    @After
    public void clearUsers() throws Exception {
        userDtoService.removeAll();
    }

}
