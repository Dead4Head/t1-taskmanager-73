package ru.t1.amsmirnov.taskmanager.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.enumerated.RoleType;

import javax.persistence.*;

@Entity
@Table(name = "tm_role_web")
public final class RoleWebDto extends AbstractUserOwnedModelDto {

    @NotNull
    @Column(name = "roletype")
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    @NotNull
    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(@NotNull final RoleType roleType) {
        this.roleType = roleType;
    }

    @Override
    public String toString() {
        return roleType.name();
    }

}
