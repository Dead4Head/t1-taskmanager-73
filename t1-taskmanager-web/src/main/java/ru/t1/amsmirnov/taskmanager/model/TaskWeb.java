package ru.t1.amsmirnov.taskmanager.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "tm_project_web")
public final class TaskWeb extends AbstractUserOwnedModel {

    @Nullable
    private String name;

    @Nullable
    private String description;

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "datestart")
    private Date dateStart = new Date();

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "datefinish")
    private Date dateFinish;

    @Nullable
    @ManyToOne
    private ProjectWeb projectWeb;

    public TaskWeb() {
    }

    public TaskWeb(@NotNull final String name) {
        this.name = name;
    }

    @Nullable
    public String getName() {
        return name;
    }

    public void setName(@Nullable final String name) {
        this.name = name;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable final String description) {
        this.description = description;
    }

    @NotNull
    public Status getStatus() {
        return status;
    }

    public void setStatus(@NotNull final Status status) {
        this.status = status;
    }

    @Nullable
    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(@Nullable final Date dateStart) {
        this.dateStart = dateStart;
    }

    @Nullable
    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(@Nullable final Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    @Nullable
    public ProjectWeb getProjectWeb() {
        return projectWeb;
    }

    public void setProjectWeb(@Nullable final ProjectWeb projectWeb) {
        this.projectWeb = projectWeb;
    }

}
