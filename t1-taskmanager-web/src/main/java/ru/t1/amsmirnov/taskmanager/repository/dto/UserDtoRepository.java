package ru.t1.amsmirnov.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.UserWebDto;

public interface UserDtoRepository extends AbstractDtoRepository<UserWebDto> {

    @Nullable
    UserWebDto findByLogin(@NotNull String login);

    boolean existsByLogin(@NotNull String login);

}
