package ru.t1.amsmirnov.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.amsmirnov.taskmanager.model.AbstractModel;

import java.util.Optional;

@NoRepositoryBean
public interface AbstractRepository<M extends AbstractModel> extends JpaRepository<M, String> {

    void deleteById(@NotNull final String id);

    @NotNull
    Page<M> findAll(@NotNull Pageable pageable);

    @NotNull
    Optional<M> findById(@NotNull final String id);

    boolean existsById(@NotNull final String id);

}
