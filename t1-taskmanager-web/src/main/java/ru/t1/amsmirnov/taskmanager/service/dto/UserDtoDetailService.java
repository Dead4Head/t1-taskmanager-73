package ru.t1.amsmirnov.taskmanager.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.amsmirnov.taskmanager.dto.CustomUser;
import ru.t1.amsmirnov.taskmanager.dto.RoleWebDto;
import ru.t1.amsmirnov.taskmanager.dto.UserWebDto;
import ru.t1.amsmirnov.taskmanager.enumerated.RoleType;
import ru.t1.amsmirnov.taskmanager.repository.dto.RoleDtoRepository;
import ru.t1.amsmirnov.taskmanager.repository.dto.UserDtoRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
@Primary
public class UserDtoDetailService implements UserDetailsService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDtoRepository userRepository;

    @Autowired
    private RoleDtoRepository roleRepository;

    @Override
    public UserDetails loadUserByUsername(@NotNull final String username) throws UsernameNotFoundException {

        System.out.println("USER FIND" + username);
        @Nullable final UserWebDto user = userRepository.findByLogin(username);
        System.out.println("USER FOUND" + username);

        if (user == null) throw new UsernameNotFoundException(username);
        System.out.println("USER FOUND" + user.getId());
        @NotNull final List<RoleWebDto> userRoles = roleRepository.findAllByUserId(user.getId());
        @NotNull List<String> roles = new ArrayList<>();
        for (@NotNull final RoleWebDto role : userRoles) roles.add(role.toString());

        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(roles.toArray(new String[]{}))
                .build()).withUserId(user.getId());
    }

    private void initUser(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final RoleType roleType
    ) {
        @Nullable final UserWebDto user = userRepository.findByLogin(login);
        if (user != null) return;
        createUser(login, password, roleType);
    }

    @PostConstruct
    private void init() {
        initUser("admin", "admin", RoleType.ADMIN);
        initUser("test", "test", RoleType.USER);
    }

    @Transactional
    public void createUser(final String login, final String password, final RoleType roleType) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        final String passwordHash = passwordEncoder.encode(password);
        final UserWebDto user = new UserWebDto();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        final RoleWebDto role = new RoleWebDto();
        role.setUserId(user.getId());
        role.setRoleType(roleType);
        userRepository.save(user);
        roleRepository.save(role);
    }


}
