package ru.t1.amsmirnov.taskmanager.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.amsmirnov.taskmanager.dto.CustomUser;
import ru.t1.amsmirnov.taskmanager.enumerated.RoleType;
import ru.t1.amsmirnov.taskmanager.model.RoleWeb;
import ru.t1.amsmirnov.taskmanager.model.UserWeb;
import ru.t1.amsmirnov.taskmanager.repository.model.UserRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
public class UserDetailService implements UserDetailsService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(@NotNull final String username) throws UsernameNotFoundException {
        @Nullable final UserWeb user = userRepository.findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);
        @NotNull final List<RoleWeb> userRoles = user.getRoles();
        @NotNull List<String> roles = new ArrayList<>();
        for (@NotNull final RoleWeb role : userRoles) roles.add(role.toString());
        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(roles.toArray(new String[]{}))
                .build()).withUserId(user.getId());
    }

    private void initUser(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final RoleType roleType
    ) {
        @Nullable final UserWeb user = userRepository.findByLogin(login);
        if (user != null) return;
        createUser(login, password, roleType);
    }

    @PostConstruct
    private void init() {
        initUser("admin", "admin", RoleType.ADMIN);
        initUser("test", "test", RoleType.USER);
    }

    @Transactional
    public void createUser(final String login, final String password, final RoleType roleType) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        final String passwordHash = passwordEncoder.encode(password);
        final UserWeb user = new UserWeb();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        final RoleWeb role = new RoleWeb();
        role.setUser(user);
        role.setRoleType(roleType);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
    }

}